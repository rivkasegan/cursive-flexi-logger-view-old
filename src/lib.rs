//! # A `FlexiLoggerView` for cursive
//!
//! This crate provides a new debug view for
//! [gyscos/cursive](https://github.com/gyscos/cursive) using the
//! [emabee/flexi_logger](https://github.com/emabee/flexi_logger) crate. This
//! enables the `FlexiLoggerView` to respect the `RUST_LOG` environment variable
//! as well as the `flexi_logger` configuration file. Have a look at the `demo`
//! below to see how it looks.
//!
//! ## Using the `FlexiLoggerView`
//!
//! To create a `FlexiLoggerView` you first have to register the
//! `cursive_flexi_logger` as a `LogTarget` in `flexi_logger`. After the
//! `flexi_logger` has started, you may create a `FlexiLoggerView` instance and
//! add it to cursive.
//!
//! ```rust
//! use cursive::{Cursive, CursiveExt};
//! use cursive_flexi_logger_view::FlexiLoggerView;
//! use flexi_logger::Logger;
//!
//!     // we need to initialize cursive first, as the cursive-flexi-logger
//!     // needs a cursive callback sink to notify cursive about screen refreshs
//!     // when a new log message arrives
//!     let mut siv = Cursive::default();
//!
//!     Logger::try_with_env_or_str("trace")
//!         .expect("Could not create Logger from environment :(")
//!         .log_to_file_and_writer(
//!            flexi_logger::FileSpec::default()
//!                 .directory("logs")
//!                 .suppress_timestamp(),
//!             cursive_flexi_logger_view::cursive_flexi_logger(siv.cb_sink().clone())
//!         )
//!         .format(flexi_logger::colored_with_thread)
//!         .start()
//!         .expect("failed to initialize logger!");
//!
//!     siv.add_layer(FlexiLoggerView::new_scrollable()); // omit `scrollable` to remove scrollbars
//!
//!     log::info!("test log message");
//!     // siv.run();
//! ```
//!
//! Look into the `FlexiLoggerView` documentation for a detailed explanation.
//!
//! ## Add toggleable flexi_logger debug console view
//!
//! This crate also provide utility functions, which is simplify usage of `FlexiLoggerView`, providing
//! debug console view like [`Cursive::toggle_debug_console`](/cursive/latest/cursive/struct.Cursive.html#method.toggle_debug_console).
//! There is 3 functions:
//!
//!  - `show_flexi_logger_debug_console`: show debug console view;
//!  - `hide_flexi_logger_debug_console`: hide debug console view (if visible);
//!  - `toggle_flexi_logger_debug_console`: show the debug console view, or hide it if it's already visible.
//!
//! ```rust
//! use cursive::{Cursive, CursiveExt};
//! use cursive_flexi_logger_view::{show_flexi_logger_debug_console, hide_flexi_logger_debug_console, toggle_flexi_logger_debug_console};
//! use flexi_logger::Logger;
//!
//!     // we need to initialize cursive first, as the cursive-flexi-logger
//!     // needs a cursive callback sink to notify cursive about screen refreshs
//!     // when a new log message arrives
//!     let mut siv = Cursive::default();
//!
//!     Logger::try_with_env_or_str("trace")
//!         .expect("Could not create Logger from environment :(")
//!         .log_to_file_and_writer(
//!            flexi_logger::FileSpec::default()
//!                 .directory("logs")
//!                 .suppress_timestamp(),
//!             cursive_flexi_logger_view::cursive_flexi_logger(siv.cb_sink().clone())
//!         )
//!         .format(flexi_logger::colored_with_thread)
//!         .start()
//!         .expect("failed to initialize logger!");
//!
//!     siv.add_global_callback('~', toggle_flexi_logger_debug_console);  // Bind '~' key to show/hide debug console view
//!     siv.add_global_callback('s', show_flexi_logger_debug_console);  // Bind 's' key to show debug console view
//!     siv.add_global_callback('h', hide_flexi_logger_debug_console);  // Bind 'h' key to hide debug console view
//!
//!     log::info!("test log message");
//!     // siv.run();
//! ```
#![deny(clippy::all)]

use cursive::{CbSink, Cursive, Printer, Vec2};
//use cursive_core::event::*;
use cursive_core::theme::{BaseColor, Color};
use cursive_core::utils::markup::StyledString;
use cursive_core::view::{Nameable, ScrollStrategy, Scrollable, View};
use cursive_core::views::{Dialog, ScrollView};
use flexi_logger::{writers::LogWriter, DeferredNow, Level, Record};
use std::collections::HashMap;
use std::collections::VecDeque;
use std::sync::{
    atomic::{AtomicUsize, Ordering},
    Arc, Mutex,
};
use unicode_width::UnicodeWidthStr;

struct LogBuffer {
    buffer: VecDeque<StyledString>,
    last_required_size: Vec2,
    last_required_size_dirty: bool,
    view_offsets: Vec<Arc<AtomicUsize>>,
}

static FLEXI_LOGGER_DEBUG_VIEW_NAME: &str = "_flexi_debug_view";

lazy_static::lazy_static! {
    static ref LOGS: Arc<Mutex<LogBuffer>> = Arc::new(Mutex::new(LogBuffer{
        buffer: VecDeque::<StyledString>::new(),
        last_required_size: Vec2::new(0,0),
        last_required_size_dirty: true,
        view_offsets: Vec::new(),
    }));
}

/// Push to log
fn push_to_log(line: StyledString) {
    let mut logs = LOGS.lock().unwrap();
    logs.buffer.pop_front();
    logs.buffer.push_back(line);
    logs.last_required_size_dirty = true;
    for vo in &logs.view_offsets {
        vo.fetch_add(1, Ordering::Relaxed);
    }
}

/// Parse lines to log
pub fn parse_lines_to_log(mut starting_style: cursive_core::theme::Style, lines: String) {
    for line in lines.lines() {
        let (spanned_string, end_style) =
            cursive::utils::markup::ansi::parse_with_starting_style(starting_style, line);
        push_to_log(spanned_string);
        starting_style = end_style;
    }
}

/// Clear log
pub fn clear_log() {
    let mut logs = LOGS.lock().unwrap();
    logs.buffer.clear();
    logs.last_required_size_dirty = true;
    for vo in &logs.view_offsets {
        vo.store(0, Ordering::Relaxed);
    }
}

/// Default colors
fn default_colors() -> HashMap<Level, Color> {
    let mut colors = HashMap::<Level, Color>::new();
    colors.insert(Level::Trace, Color::Dark(BaseColor::Green));
    colors.insert(Level::Debug, Color::Dark(BaseColor::Cyan));
    colors.insert(Level::Info, Color::Dark(BaseColor::Blue));
    colors.insert(Level::Warn, Color::Dark(BaseColor::Yellow));
    colors.insert(Level::Error, Color::Dark(BaseColor::Red));
    colors
}

#[derive(Debug, Clone, Default)]
pub struct FLSelection {
    pub start_line: usize,
    pub start_col: usize,
    pub end_line: usize,
    pub end_col: usize,
    pub content_offset: Vec2,
}

/// The `FlexiLoggerView` displays log messages from the `cursive_flexi_logger` log target.
/// It is safe to create multiple instances of this struct.
///
/// # Create a plain `FlexiLoggerView`
///
/// ```rust
/// use cursive::{Cursive, CursiveExt};
/// use cursive_flexi_logger_view::FlexiLoggerView;
/// use flexi_logger::Logger;
///
///     // we need to initialize cursive first, as the cursive-flexi-logger
///     // needs a cursive callback sink to notify cursive about screen refreshs
///     // when a new log message arrives
///     let mut siv = Cursive::default();
///
///     Logger::try_with_env_or_str("trace")
///         .expect("Could not create Logger from environment :(")
///         .log_to_file_and_writer(
///            flexi_logger::FileSpec::default()
///                 .directory("logs")
///                 .suppress_timestamp(),
///             cursive_flexi_logger_view::cursive_flexi_logger(siv.cb_sink().clone())
///         )
///         .format(flexi_logger::colored_with_thread)
///         .start()
///         .expect("failed to initialize logger!");
///
///     siv.add_layer(FlexiLoggerView::new()); // add a plain flexi-logger view
///
///     log::info!("test log message");
///     // siv.run();
/// ```
///
/// # Create a scrollable `FlexiLoggerView`
///
/// ```rust
/// use cursive::{Cursive, CursiveExt};
/// use cursive_flexi_logger_view::FlexiLoggerView;
/// use flexi_logger::Logger;
///
///     // we need to initialize cursive first, as the cursive-flexi-logger
///     // needs a cursive callback sink to notify cursive about screen refreshs
///     // when a new log message arrives
///     let mut siv = Cursive::default();
///
///     Logger::try_with_env_or_str("trace")
///         .expect("Could not create Logger from environment :(")
///         .log_to_file_and_writer(
///            flexi_logger::FileSpec::default()
///                 .directory("logs")
///                 .suppress_timestamp(),
///             cursive_flexi_logger_view::cursive_flexi_logger(siv.cb_sink().clone())
///         )
///         .format(flexi_logger::colored_with_thread)
///         .start()
///         .expect("failed to initialize logger!");
///
///     siv.add_layer(FlexiLoggerView::new_scrollable()); // add a scrollable flexi-logger view
///
///     log::info!("test log message");
///     // siv.run();
/// ```
pub struct FlexiLoggerView {
    pub indent: bool,
    pub selection: Mutex<FLSelection>,
    // Number of lines added since our last draw
    pub view_offset: Arc<AtomicUsize>,
}

pub trait Indentable {
    fn no_indent(self) -> Self;
    fn indent(self) -> Self;
}

impl Default for FlexiLoggerView {
    fn default() -> Self {
        Self::new()
    }
}

impl FlexiLoggerView {
    /// Create a new `FlexiLoggerView` which is wrapped in a `ScrollView`.
    pub fn new_scrollable() -> ScrollView<Self> {
        Self::new()
            .scrollable()
            .scroll_x(true)
            .scroll_y(true)
            .scroll_strategy(ScrollStrategy::StickToBottom)
    }

    /// Create a new `FlexiLoggerView`.
    pub fn new() -> Self {
        // Add a view offset
        let view_offset = Arc::new(AtomicUsize::new(0));
        let mut logs = LOGS.lock().unwrap();
        logs.view_offsets.push(view_offset.clone());
        FlexiLoggerView {
            indent: true,
            selection: Mutex::new(FLSelection::default()),
            view_offset,
        }
    }

    pub fn clear_selection(&mut self) {
        *self.selection.lock().unwrap() = FLSelection::default();
    }
}

impl Drop for FlexiLoggerView {
    fn drop(&mut self) {
        let mut logs = LOGS.lock().unwrap();
        for n in 0..logs.view_offsets.len() {
            if Arc::ptr_eq(&logs.view_offsets[n], &self.view_offset) {
                logs.view_offsets.remove(n);
                break;
            }
        }
    }
}

/// Resize the log buffer
pub fn resize(new_len: usize) {
    let mut logs = LOGS.lock().unwrap();
    logs.buffer.resize(new_len, StyledString::from(""));
    logs.last_required_size_dirty = true;
}

impl Indentable for ScrollView<FlexiLoggerView> {
    /// Changes a `FlexiLoggerView`, which is contained in a `ScrollView`, to not indent messages
    /// spanning multiple lines.
    fn no_indent(mut self) -> Self {
        self.get_inner_mut().indent = false;
        self
    }

    /// Changes a `FlexiLoggerView`, which is contained in a `ScrollView`, to indent messages
    /// spanning multiple lines.
    fn indent(mut self) -> Self {
        self.get_inner_mut().indent = true;
        self
    }
}

impl Indentable for FlexiLoggerView {
    /// Changes a `FlexiLoggerView` to not indent messages spanning multiple lines.
    fn no_indent(mut self) -> Self {
        self.indent = false;
        self
    }

    /// Changes a `FlexiLoggerView` to indent messages spanning multiple lines.
    fn indent(mut self) -> Self {
        self.indent = true;
        self
    }
}

impl View for FlexiLoggerView {
    // fn on_event(&mut self, mut e: Event) -> EventResult {
    //     match e {
    //         Event::Mouse {
    //             offset,
    //             position,
    //             event,
    //         } => match event {
    //             MouseEvent::Press(MouseButton::Left) => {
    //                 let mut sel = self.selection.lock().unwrap();
    //                 //sel.start_line = position.y + self.content_viewport();
    //                 EventResult::Consumed(None)
    //             }
    //             MouseEvent::Release(MouseButton::Left) => {
    //                 //
    //                 EventResult::Consumed(None)
    //             }
    //             _ => EventResult::Ignored,
    //         },
    //         _ => EventResult::Ignored,
    //     }
    // }

    fn draw(&self, printer: &Printer<'_, '_>) {
        let logs = LOGS.lock().unwrap();

        // Get view offset and apply to selection, and reset
        // Keep scroll/content offset as well for next selection change
        let vo = self.view_offset.swap(0, Ordering::Relaxed);
        let mut sel = self.selection.lock().unwrap();
        sel.start_line = sel.start_line.saturating_sub(vo);
        sel.end_line = sel.end_line.saturating_sub(vo);
        sel.content_offset = printer.content_offset;

        // Only print the last logs, so skip what doesn't fit
        let skipped = logs.buffer.len().saturating_sub(printer.size.y);

        let mut y = printer.content_offset.y;
        for msg in logs
            .buffer
            .iter()
            .skip(skipped + printer.content_offset.y)
            .take(printer.output_size.y)
        {
            let mut x = 0;
            for span in msg.spans() {
                printer.with_style(*span.attr, |printer| {
                    printer.print((x, y), span.content);
                });
                x += span.width;
            }
            y += 1;
        }
    }

    fn required_size(&mut self, constraint: Vec2) -> Vec2 {
        let mut logs = LOGS.lock().unwrap();
        if !logs.last_required_size_dirty {
            return logs.last_required_size;
        }

        // The longest line sets the width
        let w = logs
            .buffer
            .iter()
            .map(|msg| {
                msg.spans()
                    .map(|x|
                    // if the log message contains more than one line,
                    // only the longest line should be considered
                    // (definitely not the total content.len())
                    x.content.split('\n').map(|x| x.width()).max().unwrap())
                    .sum::<usize>()
            })
            .max()
            .unwrap_or(1);
        let h = logs
            .buffer
            .iter()
            .map(|msg| {
                msg.spans()
                    .last()
                    .map(|x| x.content.split('\n').count())
                    .unwrap_or_default()
            })
            .sum::<usize>();
        let w = std::cmp::max(w, constraint.x);
        let h = std::cmp::max(h, constraint.y);

        logs.last_required_size = Vec2::new(w, h);
        logs.last_required_size_dirty = false;
        logs.last_required_size
    }
    fn needs_relayout(&self) -> bool {
        true
    }
}

/// The `flexi_logger` `LogWriter` implementation for the `FlexiLoggerView`.
///
/// Use the `cursive_flexi_logger` function to create an instance of this struct.
pub struct CursiveLogWriter {
    sink: CbSink,
    colors: HashMap<Level, Color>,
}

impl CursiveLogWriter {
    pub fn set_colors(&mut self, colors: HashMap<Level, Color>) {
        self.colors = colors;
    }

    fn push_styled(&self, line: StyledString) -> std::io::Result<()> {
        push_to_log(line);

        self.sink.send(Box::new(|_siv| {})).map_err(|_| {
            std::io::Error::new(
                std::io::ErrorKind::BrokenPipe,
                "cursive callback sink is closed!",
            )
        })
    }

    // pub fn push(&self, level: Level, text: &str) -> std::io::Result<()> {
    //     let color = *self.colors.get(&level).unwrap();

    //     let mut line = StyledString::new();
    //     line.append_styled(text.to_string(), color);
    //     self.push_styled(line)
    // }
}

/// Creates a new `LogWriter` instance for the `FlexiLoggerView`. Use this to
/// register a cursive log writer in `flexi_logger`.
///
/// Although, it is safe to create multiple cursive log writers, it may not be
/// what you want. Each instance of a cursive log writer replicates the log
/// messages in to `FlexiLoggerView`. When registering multiple cursive log
/// writer instances, a single log messages will be duplicated by each log
/// writer.
///
/// # Registering the cursive log writer in `flexi_logger`
///
/// ```rust
/// use cursive::{Cursive, CursiveExt};
/// use flexi_logger::Logger;
///
///     // we need to initialize cursive first, as the cursive-flexi-logger
///     // needs a cursive callback sink to notify cursive about screen refreshs
///     // when a new log message arrives
///     let mut siv = Cursive::default();
///
///     Logger::try_with_env_or_str("trace")
///         .expect("Could not create Logger from environment :(")
///         .log_to_file_and_writer(
///            flexi_logger::FileSpec::default()
///                 .directory("logs")
///                 .suppress_timestamp(),
///             cursive_flexi_logger_view::cursive_flexi_logger(siv.cb_sink().clone())
///         )
///         .format(flexi_logger::colored_with_thread)
///         .start()
///         .expect("failed to initialize logger!");
/// ```
pub fn cursive_flexi_logger(cb_sink: cursive::CbSink) -> Box<CursiveLogWriter> {
    let colors = default_colors();
    Box::new(CursiveLogWriter {
        sink: cb_sink.clone(),
        colors,
    })
}

impl LogWriter for CursiveLogWriter {
    fn write(&self, _now: &mut DeferredNow, record: &Record) -> std::io::Result<()> {
        let color = *self.colors.get(&record.level()).unwrap();

        let args = format!("{}", &record.args());

        let mut line = StyledString::new();
        let mut indent = 0;
        let levstr = format!("{}: ", record.level());
        indent += levstr.len();
        line.append_styled(levstr, color);
        let filestr = format!(
            "{}:{} ",
            record.file().unwrap_or("(unnamed)"),
            record.line().unwrap_or(0),
        );
        indent += filestr.len();
        line.append_plain(filestr);

        for argline in args.lines() {
            line.append_styled(argline, color);
            self.push_styled(line)?;
            line = StyledString::new();
            line.append_plain(" ".repeat(indent));
        }
        Ok(())
    }

    fn flush(&self) -> std::io::Result<()> {
        // we are not buffering
        Ok(())
    }

    fn max_log_level(&self) -> log::LevelFilter {
        log::LevelFilter::max()
    }
}

/// Show the flexi_logger debug console.
///
/// This is analog to [`Cursive::show_debug_console`](/cursive/latest/cursive/struct.Cursive.html#method.show_debug_console).
///
/// # Add binding to show flexi_logger debug view
///
/// ```rust
/// use cursive::{Cursive, CursiveExt};
/// use cursive_flexi_logger_view::show_flexi_logger_debug_console;
/// use flexi_logger::Logger;
///
///     // we need to initialize cursive first, as the cursive-flexi-logger
///     // needs a cursive callback sink to notify cursive about screen refreshs
///     // when a new log message arrives
///     let mut siv = Cursive::default();
///
///     Logger::try_with_env_or_str("trace")
///         .expect("Could not create Logger from environment :(")
///         .log_to_file_and_writer(
///            flexi_logger::FileSpec::default()
///                 .directory("logs")
///                 .suppress_timestamp(),
///             cursive_flexi_logger_view::cursive_flexi_logger(siv.cb_sink().clone())
///         )
///         .format(flexi_logger::colored_with_thread)
///         .start()
///         .expect("failed to initialize logger!");
///
///     siv.add_global_callback('~', show_flexi_logger_debug_console);  // Add binding to show flexi_logger debug view
///
///     // siv.run();
/// ```
pub fn show_flexi_logger_debug_console(siv: &mut Cursive) {
    siv.add_layer(
        Dialog::around(FlexiLoggerView::new_scrollable().with_name(FLEXI_LOGGER_DEBUG_VIEW_NAME))
            .title("Debug console"),
    );
}

/// Hide the flexi_logger debug console (if visible).
///
/// # Add binding to hide flexi_logger debug view
///
/// ```rust
/// use cursive::{Cursive, CursiveExt};
/// use cursive_flexi_logger_view::hide_flexi_logger_debug_console;
/// use flexi_logger::Logger;
///
///     // we need to initialize cursive first, as the cursive-flexi-logger
///     // needs a cursive callback sink to notify cursive about screen refreshs
///     // when a new log message arrives
///     let mut siv = Cursive::default();
///
///     Logger::try_with_env_or_str("trace")
///         .expect("Could not create Logger from environment :(")
///         .log_to_file_and_writer(
///            flexi_logger::FileSpec::default()
///                 .directory("logs")
///                 .suppress_timestamp(),
///             cursive_flexi_logger_view::cursive_flexi_logger(siv.cb_sink().clone())
///         )
///         .format(flexi_logger::colored_with_thread)
///         .start()
///         .expect("failed to initialize logger!");
///
///     siv.add_global_callback('~', hide_flexi_logger_debug_console);  // Add binding to hide flexi_logger debug view
///
///     // siv.run();
/// ```
pub fn hide_flexi_logger_debug_console(siv: &mut Cursive) {
    if let Some(pos) = siv
        .screen_mut()
        .find_layer_from_name(FLEXI_LOGGER_DEBUG_VIEW_NAME)
    {
        siv.screen_mut().remove_layer(pos);
    }
}

/// Show the flexi_logger debug console, or hide it if it's already visible.
///
/// This is analog to [`Cursive::toggle_debug_console`](/cursive/latest/cursive/struct.Cursive.html#method.toggle_debug_console).
///
/// # Enable toggleable flexi_logger debug view
///
/// ```rust
/// use cursive::{Cursive, CursiveExt};
/// use cursive_flexi_logger_view::toggle_flexi_logger_debug_console;
/// use flexi_logger::Logger;
///
///     // we need to initialize cursive first, as the cursive-flexi-logger
///     // needs a cursive callback sink to notify cursive about screen refreshs
///     // when a new log message arrives
///     let mut siv = Cursive::default();
///
///     Logger::try_with_env_or_str("trace")
///         .expect("Could not create Logger from environment :(")
///         .log_to_file_and_writer(
///            flexi_logger::FileSpec::default()
///                 .directory("logs")
///                 .suppress_timestamp(),
///             cursive_flexi_logger_view::cursive_flexi_logger(siv.cb_sink().clone())
///         )
///         .format(flexi_logger::colored_with_thread)
///         .start()
///         .expect("failed to initialize logger!");
///
///     siv.add_global_callback('~', toggle_flexi_logger_debug_console);  // Enable toggleable flexi_logger debug view
///
///     // siv.run();
/// ```
pub fn toggle_flexi_logger_debug_console(siv: &mut Cursive) {
    if let Some(pos) = siv
        .screen_mut()
        .find_layer_from_name(FLEXI_LOGGER_DEBUG_VIEW_NAME)
    {
        siv.screen_mut().remove_layer(pos);
    } else {
        show_flexi_logger_debug_console(siv);
    }
}
